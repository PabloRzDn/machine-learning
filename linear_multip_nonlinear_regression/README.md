# Regresión

Pablo Ruz Donoso, febrero 2022

---
### Resumen

El programa contenido en este directorio entrega los resultados de la caracterización de la curva o superficie de ajuste y los principales indicadores, según un marco de datos entregado (dafaframe) en formato .csv, y en base al modelo matemático de regresión. En este caso, se pueden aplicar los modelos de Regresión lineal simple, múltiple y regresión no lineal.

1. **Regresión Lineal Simple**:
   Permite la selección de las variables independiente y dependiente del estudio, a partir de las columnas de datos dentro del dataframe. Los resultados obtenidos son el coeficiente y la intersección de la recta obtenida por la regresión, y los indicadores del error absoluto medio, error cuadrado medio, y coeficiente de correlación.

2. **Regresión Lineal Múltiple**: 
Permite la selección de múltiples variables independientes (ingresando la cantidad de variables a estudiar) y la dependiente. Los resultados obtenidos son los coeficientes de la curva o superficie (según la cantidad de variables), el error absoluto medio, error cuadrado medio, la varianza y el coeficiente de correlación.

3. **Regresión no lineal (polinómica)**: 
Permite la selecciónde las variables independiente y dependiente del estudio, además del grado del polinomio al cual se desea ajustar. Los resultados obtenidos son los coeficientes, intersección, el error cuadrado medio, error absoluto medio y el coeficiente de correlación.

### Estructura

El directorio está compuesto por los script *main.py* el cual ejecuta el programa principal, y *regression_algorithms.py*, en el cual se encuentran las clases creadas, además del dataframe crudo elegido en formato csv.

```
/linear_multip_nonlinear_regression
    documento_csv.csv
    main.py
    regression_algorithms.py
```
Las clases corresponden a:

- Dataframe: configura el dataframe para el análisis
- Regresion (hereda de Dataframe): genera el modelo
- Lineal (hereda de Regresion): genera los coeficientes, intersección, indicadores y gráficos del ajuste de regresión lineal.
- MultiLineal (hereda de Regresion): genera los coeficientes, intersección, indicadores y gráficos del ajuste de regresión multilineal.
- Polinomial (hereda de Regresion): genera los coeficientes, intersección, indicadores y gráficos del ajuste de regresión polinómica.

### Ejecución

Una vez configurado el entorno virtual e instalado los requerimientos (contenidos en *requirements.txt*), correr el programa con:

```
$ python main.py
```

### Ejemplos de Salida

```
  Pablo Ruz Donoso. Febrero 2022
            =================================================================================
            Regresión lineal simple, múltiple y no lineal

            Este script obtiene los indicadores típicos (Error Absoluto Medio -MAE-, Error 
            Cuadrado Medio -MSE-, coeficiente de correlación -r2-, varianza, entre otros), 
            además de la caracterización de la curva o superficie y gráfico, según el estudio 
            elegido.


            =================================================================================
            
1. Modelo de regresión lineal simple
2. Modelo de regresión lineal Múltiple
3. Modelo de regresión no lineal (polinómico)
4. Salir

[+] Selecciona estudio a realizar: 3
[+] Resumen de dataframe: 

 Archivo: FuelConsumption.csv | filas: 1067 | columnas: 13 | variables: ['MODELYEAR', 'MAKE', 'MODEL', 'VEHICLECLASS', 'ENGINESIZE', 'CYLINDERS', 'TRANSMISSION', 'FUELTYPE', 'FUELCONSUMPTION_CITY', 'FUELCONSUMPTION_HWY', 'FUELCONSUMPTION_COMB', 'FUELCONSUMPTION_COMB_MPG', 'CO2EMISSIONS'] 

[+] Ingrese la variable independiente (x) del estudio: enginesize
[+] Ingrese la variable dependiente (y) del estudio: co2emissions
[+]Ingrese grado del polinomio: 3
 [+] Extracto de datos de dataframe ajustado: 

                ENGINESIZE  CO2EMISSIONS
0         2.0           196
1         2.4           221
2         1.5           136
3         3.5           255
4         3.5           244 

╒═════════════════════════════════════════════════════╤════════════════╕
│ Coeficientes                                        │ intersección   │
├─────────────────────────────────────────────────────┼────────────────┤
│ [[ 0.         29.36452856  4.49472868 -0.51992244]] │ [129.74282385] │
╘═════════════════════════════════════════════════════╧════════════════╛
╒════════════════════╤═══════════════════╤════════════════════╕
│ MAE                │ MSE               │ r2                 │
├────────────────────┼───────────────────┼────────────────────┤
│ 21.073882948643554 │ 763.6057161589778 │ 0.8032049960370481 │
╘════════════════════╧═══════════════════╧════════════════════╛
```


![Figura1](Figure_1.png)