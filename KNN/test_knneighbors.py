from turtle import shape
import unittest
from failure import failure

from numpy import int64
from knneighbors import Dataframe
import pandas as pd
from pandas.util.testing import assert_frame_equal, assert_class_equal


class TestDataframe(unittest.TestCase):
    
    
    def setUp(self):
        self.archivo="teleCust1000t.csv"
        self.df=Dataframe(self.archivo)
        self.data=["region","tenure"]

    def test__str__(self):    
        self.assertEqual(type(self.df.__str__()),str)
        
        

    def test_ajustar_dataframe(self):      
        self.test_df=pd.read_csv(self.archivo)      
        assert_frame_equal(self.test_df[self.data],self.df.ajustar_dataframe(data=self.data))

if __name__=="__main__":
    unittest.main()