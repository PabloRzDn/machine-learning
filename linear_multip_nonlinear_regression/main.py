import os, sys
from regression_algorithms import Lineal, MultiLineal, Polinomial 
from tabulate import tabulate



def buscar_csv():
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(".csv")]:
            csv=str(os.path.join(filename)) 
    return csv


def seleccion_menu():
    esentero=False
    while (not esentero):
        try:
            seleccion=int(input("[+] Selecciona estudio a realizar: "))
            esentero=True
        except ValueError:
            print("[!] Ingresa un número entero")
    return seleccion

def regresion_lineal(archivo):
    dataframe=Lineal(archivo)
    print("[+] Resumen de dataframe: \n\n", dataframe.__str__(),"\n")
    ind=str(input("[+] Ingrese la variable independiente (x) del estudio: ")).upper()
    dep=str(input("[+] Ingrese la variable dependiente (y) del estudio: ")).upper()
    df_acotado=dataframe.ajustar_dataframe(ind=ind,dep=dep)
    print(''' [+] Extracto de datos de dataframe ajustado: \n
            ''',df_acotado,"\n")
    dataframe.generar_modelo(ind,dep)
    coeficiente, intercepto=dataframe.coeficientes_e_interseccion()
    caract_curva=[["Coeficientes", "intersección"],[coeficiente,intercepto]]
    

    MAE, MSE, r2=dataframe.indicadores()
    indicadores=[["MAE","MSE","r2"],[MAE,MSE,r2]]
    print(tabulate(caract_curva,tablefmt="fancy_grid"))
    print(tabulate(indicadores,tablefmt="fancy_grid"))
    dataframe.generar_grafico()

def regresion_multilineal(archivo):
    dataframe=MultiLineal(archivo)
    
    lista_var=[]
    ind=[]
    print("[+] Resumen de dataframe: \n\n", dataframe.__str__(),"\n")
    n_var=int(input("[+] Ingrese número de variables independientes (x): "))
    for i in range(n_var):
        var=str(input("\n[+] Ingrese la {}° variable independiente (x): ".format(i+1)).upper())
        lista_var.append(var)
        ind.append(var)
        
    dep=str(input("[+] Ingrese la variable dependiente (y) del estudio: ")).upper()
    
    lista_var.append(dep)
    df_acotado=dataframe.ajustar_dataframe(value=lista_var)
    print(''' [+] Extracto de datos de dataframe ajustado: \n
            ''',df_acotado,"\n")
    
    dataframe.generar_modelo(ind,dep)
    coeficientes, intercepto=dataframe.coeficientes_e_interseccion()
    caract_superficie=[["Coeficientes", "intersección"],[coeficientes,intercepto]]
    MAE, MSE, r2, varianza=dataframe.indicadores()
   
    indicadores=[["MAE","MSE","r2","varianza"],[MAE,MSE,r2,varianza]]
    print(tabulate(caract_superficie,tablefmt="fancy_grid"))
    print(tabulate(indicadores,tablefmt="fancy_grid"))


def regresion_polinomial(archivo):
    
    dataframe=Polinomial(archivo)
    print("[+] Resumen de dataframe: \n\n", dataframe.__str__(),"\n")
    ind=str(input("[+] Ingrese la variable independiente (x) del estudio: ")).upper()
    dep=str(input("[+] Ingrese la variable dependiente (y) del estudio: ")).upper()
    grado=int(input("[+]Ingrese grado del polinomio: "))
    df_acotado=dataframe.ajustar_dataframe(ind=ind,dep=dep)
    print(''' [+] Extracto de datos de dataframe ajustado: \n
            ''',df_acotado,"\n")

    dataframe.generar_modelo(ind,dep)
    coeficientes, intercepto=dataframe.coeficientes_e_interseccion(grado)
    MAE, MSE, r2=dataframe.indicadores()
    caract_curva=[["Coeficientes", "intersección"],[coeficientes,intercepto]]

    indicadores=[["MAE","MSE","r2"],[MAE,MSE,r2]]

    print(tabulate(caract_curva,tablefmt="fancy_grid"))
    print(tabulate(indicadores,tablefmt="fancy_grid"))
    dataframe.generar_grafico()



if __name__=="__main__":

   
  
    try:
        salir=False
        opcion=0

        titulo='''
            Pablo Ruz Donoso. Febrero 2022
            =================================================================================
            Regresión lineal simple, múltiple y no lineal

            Este script obtiene los indicadores típicos (Error Absoluto Medio -MAE-, Error 
            Cuadrado Medio -MSE-, coeficiente de correlación -r2-, varianza, entre otros), 
            además de la caracterización de la curva o superficie y gráfico, según el estudio 
            elegido.


            =================================================================================
            '''
        print(titulo)
        csv=buscar_csv()
        
        while(not salir):

            print("1. Modelo de regresión lineal simple")
            print("2. Modelo de regresión lineal Múltiple")
            print("3. Modelo de regresión no lineal (polinómico)")
            print("4. Salir\n")

            opcion=seleccion_menu()
            if opcion==1:
                regresion_lineal(csv)
            elif opcion==2:
                regresion_multilineal(csv)
            elif opcion==3:    
                regresion_polinomial(csv)
            elif opcion==4:
                
                salir=True
            else:
                print("[!] Elige una opción entre 1 y 3")

        print("[-] Fin de programa")
    except KeyboardInterrupt:
        print("[-] Interrupción del programa")
    
    