import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import r2_score

from abc import ABCMeta, abstractmethod

class metaDataframe(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        pass
    @abstractmethod
    def __str__(self):
        pass
    @abstractmethod
    def ajustar_dataframe(self):
        pass


class metaRegresion(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        pass
    @abstractmethod
    def generar_modelo(self):
        pass

class metaLineal(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        pass
    @abstractmethod
    def coeficientes_e_interseccion(self):
        pass
    @abstractmethod
    def generar_grafico(self):
        pass
    @abstractmethod
    def indicadores(self):
        pass

class metaMultiLineal(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        pass
    @abstractmethod
    def coeficientes_e_interseccion(self):
        pass
    @abstractmethod
    def indicadores(self):
        pass


class metaPolinomial(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        pass
    @abstractmethod
    def coeficientes_e_interseccion(self):
        pass
    @abstractmethod
    def generar_grafico(self):
        pass
    @abstractmethod
    def indicadores(self):
        pass




class Dataframe(metaDataframe):
    def __init__(self, arch_temp):
        """ Constructor de la clase, recibe el raw dataframe en formato csv """
        
        self.arch_temp=arch_temp
        self.df=pd.read_csv(self.arch_temp)
        self.cols_df=list(self.df.columns)
        self.count_rows=self.df.shape[0]
        self.count_cols=self.df.shape[1]
    
    def __str__(self)-> str:
        """ Retorna un string con los datos del objeto """
        return "Archivo: {} | filas: {} | columnas: {} | variables: {}".format(self.arch_temp,self.count_rows,self.count_cols,self.cols_df)
    
    
    def ajustar_dataframe(self,**kwargs):
        """ Recibe las variables seleccionadas por el usuario y acota el dataframe ingreaso"""
        
        self.vars=[]
        for key, values in kwargs.items():
            if(type(values)==list):
                self.cols_vars=self.df[values]
                return self.cols_vars
            else:
                self.vars.append(values)
        for i in range(len(self.vars)):
            if self.vars[i] not in self.cols_df:
                sys.exit("[!] la variable {} no existe en el dataframe.".format(self.vars[i]))
        
        self.cols_vars=self.df[self.vars]
        return self.cols_vars.head(5)


class Regresion(metaRegresion,Dataframe):
    def __init__(self,arch_temp):
        Dataframe.__init__(self,arch_temp)

    def generar_modelo(self,var_ind,var_dep):
        self.var_ind, self.var_dep = var_ind, var_dep
        self.mascara=np.random.rand(len(self.df)) < 0.8
        self.entren_interv=self.cols_vars[self.mascara]
        self.test_interv=self.cols_vars[~self.mascara]

        if isinstance(self.var_ind,list):
            
            self.train_x_arr=np.asanyarray(self.entren_interv[self.var_ind])
            self.train_y_arr=np.asanyarray(self.entren_interv[self.var_dep])

            self.test_x_arr=np.asanyarray(self.test_interv[self.var_ind])
            self.test_y_arr=np.asanyarray(self.test_interv[self.var_dep])
        else:
            self.train_x_arr=np.asanyarray(self.entren_interv[[self.var_ind]])
            self.train_y_arr=np.asanyarray(self.entren_interv[[self.var_dep]])

            self.test_x_arr=np.asanyarray(self.test_interv[[self.var_ind]])
            self.test_y_arr=np.asanyarray(self.test_interv[[self.var_dep]])


class Lineal(metaLineal,Regresion):
    def __init__(self,arch_temp):
        Regresion.__init__(self,arch_temp)

    def coeficientes_e_interseccion(self):
        self.lineal=linear_model.LinearRegression()
        self.lineal.fit(self.train_x_arr,self.train_y_arr)
        self.coef, self.intersec=self.lineal.coef_[0][0], self.lineal.intercept_[0]
        return self.coef, self.intersec

    def indicadores(self):
        self.test_y_=self.lineal.predict(self.test_x_arr)
        self.MAE=np.mean(np.absolute(self.test_y_-self.test_y_arr))
        self.MSE=np.mean((self.test_y_-self.test_y_arr)**2)
        self.r2=r2_score(self.test_y_arr,self.test_y_)
        return self.MAE, self.MSE, self.r2
    def generar_grafico(self):
        """Muestra el gráfico de la data usalda para el entrenamiento, y la curva de ajuste"""
        plt.scatter(self.entren_interv[self.var_ind],self.entren_interv[self.var_dep], color="blue")
        plt.plot(self.train_x_arr,self.coef*self.train_x_arr+self.intersec, "-r")
        plt.xlabel(self.var_ind)
        plt.ylabel(self.var_dep)
        plt.show()


class MultiLineal(metaMultiLineal,Regresion):
    def __init__(self,arch_temp):
        Regresion.__init__(self,arch_temp)

    def coeficientes_e_interseccion(self):
        self.multilineal=linear_model.LinearRegression()
        self.multilineal.fit(self.train_x_arr,self.train_y_arr)
        self.coef, self.intersec=self.multilineal.coef_, self.multilineal.intercept_
        return self.coef, self.intersec

    def indicadores(self):
        self.test_y_=self.multilineal.predict(self.test_x_arr)
        self.MAE=np.mean(np.absolute(self.test_y_-self.test_y_arr))
        self.MSE=np.mean((self.test_y_-self.test_y_arr)**2)
        self.variance=self.multilineal.score(self.test_x_arr,self.test_y_arr)
        self.r2=r2_score(self.test_y_arr,self.test_y_)
        return self.MAE, self.MSE, self.r2, self.variance


class Polinomial(metaPolinomial,Regresion):
    def __init__(self,arch_temp):
        Regresion.__init__(self,arch_temp)


    def coeficientes_e_interseccion(self,grado):

        """ Realiza el ajuste polinomial dependiendo del grado, y se genera la curva"""
        self.grado=grado
        self.poly=PolynomialFeatures(degree=self.grado)
        self.train_x_poly=self.poly.fit_transform(self.train_x_arr)
        self.clf=linear_model.LinearRegression()
        self.train_y=self.clf.fit(self.train_x_poly,self.train_y_arr)
        self.XX=np.arange(0.0,10.0,0.1)
        
        self.YY=self.clf.intercept_[0]
        for i in range(grado):
            self.YY+=self.clf.coef_[0][i+1]*np.power(self.XX,i+1)
        
        return self.clf.coef_, self.clf.intercept_
    def generar_grafico(self):
        """Muestra el gráfico de la data usalda para el entrenamiento, y la curva de ajuste"""
        plt.scatter(self.entren_interv[self.var_ind],self.entren_interv[self.var_dep], color="blue")
        plt.plot(self.XX,self.YY, "-r")
        plt.xlabel(self.var_ind)
        plt.ylabel(self.var_dep)
        plt.show()

    def indicadores(self):
        """ Calcula y retorna los indicadores de error absoluto medio, error cuadrado medio y coef de correlacion"""
        self.test_x_poly=self.poly.transform(self.test_x_arr)
        self.test_y_=self.clf.predict(self.test_x_poly)
        self.MAE=np.mean(np.absolute(self.test_y_-self.test_y_arr))
        self.MSE=np.mean((self.test_y_-self.test_y_arr)**2)
        self.r2=r2_score(self.test_y_arr,self.test_y_)
        return self.MAE, self.MSE, self.r2
