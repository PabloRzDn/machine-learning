'''
Dataset compuesto por los diversos servicios y patrones de uso de clientes de una empresa de telecomunicaciones.
Se busca construir un modelo para predecir las clases de los nuevos casos desconocidos.

'''

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import preprocessing, metrics
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

#Carga de dataframe

df=pd.read_csv("teleCust1000t.csv")

#Análisis de la data

df["custcat"].value_counts()
df.hist(column="income",bins=50)

#se convierte la data en un arreglo para numpy
X=df[["region","tenure","age","marital","address","income","ed","employ","retire","gender","reside"]].values
y=df["custcat"].values

#Se normaliza la data
X=preprocessing.StandardScaler().fit(X).transform(X.astype(float))

#train test split

X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.2,random_state=4)
print("Train Set:", X_train.shape, y_train.shape)
print("Test Set:", X_test.shape,y_test.shape)


#entrenar

k=4
neigh=KNeighborsClassifier(n_neighbors=k).fit(X_train,y_train)
print(neigh)

#predecir
yhat=neigh.predict(X_test)
print(yhat[0:5])

#evaluación de la precisión

print("Train set accuracy:", metrics.accuracy_score(y_train,neigh.predict(X_train)))
print("Test set Accuracy:",metrics.accuracy_score(y_test,yhat))