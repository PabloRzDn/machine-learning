import matplotlib.pyplot as plt
import pandas as ps
import pylab as pl
import numpy as np
from sklearn import linear_model


dataf=ps.read_csv("FuelConsumption.csv")


#Selecciona determinadas variables para el analisis
cdf=dataf[["ENGINESIZE","CYLINDERS","FUELCONSUMPTION_CITY","FUELCONSUMPTION_HWY","FUELCONSUMPTION_COMB","CO2EMISSIONS"]]

'''
plt.scatter(cdf.ENGINESIZE,cdf.CO2EMISSIONS,color="blue")
plt.xlabel("Engine Size")
plt.ylabel("CO2 Emissions")
plt.show()
'''
#se crea una "máscara" para seleccionar filas aleatorias
mask=np.random.rand(len(dataf)) < 0.8

#se asocian las filas aleatorias tanto al dataset de entrenamiento, como al de testing
train=cdf[mask]
test=cdf[~mask]

'''
plt.scatter(train.ENGINESIZE, train.CO2EMISSIONS, color="blue")
plt.xlabel("Engine Size")
plt.ylabel("CO2 Emissions")
plt.show()
'''

regr=linear_model.LinearRegression()

x=np.asanyarray(train[["ENGINESIZE","CYLINDERS","FUELCONSUMPTION_COMB"]])
y=np.asanyarray(train[["CO2EMISSIONS"]])
regr.fit(x,y)

print(x)


print("Coefficients: ", regr.coef_)


y_hat=regr.predict(test[["ENGINESIZE","CYLINDERS","FUELCONSUMPTION_COMB"]])
x=np.asanyarray(test[["ENGINESIZE","CYLINDERS","FUELCONSUMPTION_COMB"]])
y=np.asanyarray(test[["CO2EMISSIONS"]])
print("Residual sum of squares:", np.mean((y_hat-y)**2))
print("Variance Score:", regr.score(x,y))