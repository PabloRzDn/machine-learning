from socket import CAN_RAW_FD_FRAMES
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pylab as pl

from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.metrics import r2_score

dataf=pd.read_csv("FuelConsumption.csv")
cdf=dataf[["ENGINESIZE","CYLINDERS","FUELCONSUMPTION_COMB","CO2EMISSIONS"]]


msk=np.random.rand(len(dataf)) < 0.8

train=cdf[msk]
test=cdf[~msk]


train_x=np.asanyarray(train[["ENGINESIZE"]])
train_y=np.asanyarray(train[["CO2EMISSIONS"]])

test_x=np.asanyarray(test[["ENGINESIZE"]])
test_y=np.asanyarray(test[["CO2EMISSIONS"]])

poly=PolynomialFeatures(degree=2)
train_x_poly=poly.fit_transform(train_x)

clf=linear_model.LinearRegression()
train_y_=clf.fit(train_x_poly,train_y)

print("Coefficients:", clf.coef_)
print("intercept:", clf.intercept_)

plt.scatter(train.ENGINESIZE,train.CO2EMISSIONS, color="blue")
XX=np.arange(0.0,10.0,0.1)

yy=clf.intercept_[0]+clf.coef_[0][1]*XX+clf.coef_[0][2]*np.power(XX,2)
plt.plot(XX,yy, "-r")
plt.xlabel("ENGINE SIZE")
plt.ylabel("CO2 EMISSIONS")


test_x_poly=poly.transform(test_x)
test_y_=clf.predict(test_x_poly)
print("MAE:", np.mean(np.absolute(test_y_-test_y)))
print("MSE:", np.mean((test_y_-test_y)**2))
print("r2", r2_score(test_y,test_y_))

plt.show()

