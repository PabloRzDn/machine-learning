import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pyparsing import col
from sklearn import preprocessing, metrics
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from abc import ABCMeta, abstractmethod
import sys
class metaDataframe(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        pass
    @abstractmethod
    def __str__(self):
        pass
    @abstractmethod
    def columnas(self):
        pass
    @abstractmethod
    def ajustar_dataframe(self):
        pass

class metaKNNeighbors(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        pass
    @abstractmethod
    def conformar_modelo(self):
        pass
    @abstractmethod
    def entrenar_modelo(self):
        pass
    @abstractmethod
    def optimizar_k(self):
        pass
  

class Dataframe(metaDataframe):
    def __init__(self,arch_temp):

        self.arch_temp=arch_temp
        self.df=pd.read_csv(self.arch_temp)
        self.cols_df=list(self.df.columns)
        self.count_rows=self.df.shape[0]
        self.count_cols=self.df.shape[1]
    
    def __str__(self)-> str:
        try:
            """ Retorna un string con los datos del objeto """
            return "Archivo: {} | filas: {} | columnas: {} | variables: {}".format(self.arch_temp,self.count_rows,self.count_cols,self.cols_df)
        except:
            raise FileNotFoundError("Archivo no existe o no encontrado")    
    
    def columnas(self):
        return self.cols_df

    def ajustar_dataframe(self,dataset,val_pred):
        """ Recibe las variables seleccionadas por el usuario y acota el dataframe ingreaso"""
        
        self.X=self.df[dataset].values
        self.y=self.df[val_pred].values

        self.X=preprocessing.StandardScaler().fit(self.X).transform(self.X.astype(float))

        self.df.hist(column=val_pred)
        plt.show()
        return self.df[val_pred].value_counts()



class KNNeighbors(metaKNNeighbors,Dataframe):
    def __init__(self,arch_temp):
        Dataframe.__init__(self,arch_temp)

    def conformar_modelo(self,proporcion_muestra):
        
        if proporcion_muestra>0.0 and proporcion_muestra < 1.0:
            self.proporcion_muestra=proporcion_muestra
            self.X_train, self.X_test, self.y_train, self.y_test=train_test_split(self.X,self.y,test_size=self.proporcion_muestra,random_state=4)
        else:
            ValueError("La proporción de la muestra debe ser un float entre 0.0 y 1.0")
        
        print("[OUT] Set de entrenamiento:", self.X_train.shape, self.y_train.shape)
        print("[OUT] Set de testeo:", self.X_test.shape,self.y_test.shape)

    def entrenar_modelo(self,K):
        self.K=K
        self.neighbors=KNeighborsClassifier(n_neighbors=self.K).fit(self.X_train,self.y_train)
        self.y_hat=self.neighbors.predict(self.X_test)
        self.precision_train=metrics.accuracy_score(self.y_train,self.neighbors.predict(self.X_train))
        self.precision_test=metrics.accuracy_score(self.y_test,self.y_hat)
        return self.precision_train, self.precision_test
    def optimizar_k(self):
        self.Ks=10
        self.mean_acc=np.zeros(self.Ks-1)
        self.std_acc=np.zeros(self.Ks-1)

        for i in range (1,self.Ks):
            self.opt_neighbors=KNeighborsClassifier(n_neighbors=i).fit(self.X_train,self.y_train)
            self.y_hat=self.opt_neighbors.predict(self.X_test)
            self.mean_acc[i-1]=metrics.accuracy_score(self.y_test,self.y_hat)
            self.std_acc=np.std(self.y_hat==self.y_test)/np.sqrt(self.y_hat.shape[0])
        
        plt.plot(range(1,self.Ks),self.mean_acc,"g")

        plt.fill_between(range(1,self.Ks),self.mean_acc-1*self.std_acc,self.mean_acc+1*self.std_acc,alpha=0.10)
        plt.fill_between(range(1,self.Ks),self.mean_acc-3*self.std_acc,self.mean_acc+3*self.std_acc,alpha=0.10, color="green")
        plt.legend(("Precisión","+/- 1 xstd","+/- 3 xstd"))
        plt.ylabel("Precisión")
        plt.xlabel("Constante K")
        plt.tight_layout()
        plt.show()
        return self.mean_acc.max(), self.mean_acc.argmax()+1

