# K-N Neighbors

Pablo Ruz Donoso, febrero 2022

---
### Resumen

El programa contenido en este directorio entrega los resultados de precisión de los intervalos de entrenamiento y testeo, bajo el modelo de K-N Neighbors, según un marco de datos entregado (dafaframe) en formato .csv. Asimismo, realiza la optimización del dataframe y las variables de estudio, determinando la mejor precisión del sistema y el valor óptimo de K.


### Estructura

El directorio está compuesto por los script *main.py* el cual ejecuta el programa principal, y *knneighbors.py*, en el cual se encuentran las clases creadas, además del dataframe crudo elegido en formato csv.

```
/linear_multip_nonlinear_regression
    documento_csv.csv
    main.py
    knneighbors.py
```
Las clases corresponden a:

- Dataframe: configura el dataframe para el análisis
- KNNeighbors (hereda de Dataframe): genera el modelo y la optimización.

### Ejecución

Una vez configurado el entorno virtual e instalado los requerimientos (contenidos en *requirements.txt*), correr el programa con:

```
$ python main.py
```

### Ejemplos de Salida

```
  [+] Resumen de dataframe: 

 Archivo: teleCust1000t.csv | filas: 1000 | columnas: 12 | variables: ['region', 'tenure', 'age', 'marital', 'address', 'income', 'ed', 'employ', 'retire', 'gender', 'reside', 'custcat'] 

[+] Ingrese el atributo de estudio para la predicción: custcat
[OUT] Resumen de Clase de estudio 
 3    281
1    266
4    236
2    217
Name: custcat, dtype: int64
[+] Ingrese proporción del tamaño de la muestra (valor entre 0.0 y 1.0):0.2
[OUT] Set de entrenamiento: (800, 11) (800,)
[OUT] Set de testeo: (200, 11) (200,)
[+] Ingresa constante K: 4
╒═════════════════════════════════╤══════════════════════════╕
│ Precisión Interv. entrenamiento │ Precisión Interv. Testeo │
├─────────────────────────────────┼──────────────────────────┤
│ 0.5475                          │ 0.32                     │
╘═════════════════════════════════╧══════════════════════════╛
===== Modelo Optimizado =====
╒═════════════════════════════╤═════════╕
│ Mejor precisión del sistema │ Valor K │
├─────────────────────────────┼─────────┤
│ 0.34                        │ 9       │
╘═════════════════════════════╧═════════╛
```



![Figura1](Figure_1.png)