import os, sys

from knneighbors import KNNeighbors
from tabulate import tabulate

def buscar_csv():
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(".csv")]:
            csv=str(os.path.join(filename)) 
    return csv


def kn_neighbors(archivo):
    
    try:
        dataframe=KNNeighbors(archivo)
        

        print("[+] Resumen de dataframe: \n\n", dataframe.__str__(),"\n")
            
        predict=str(input("[+] Ingrese el atributo de estudio para la predicción: ")).lower()
        
        cols=dataframe.columnas()
        cols.remove(predict)

        resumen_clases=dataframe.ajustar_dataframe(cols,predict)
        print("[OUT] Resumen de Clase de estudio \n", resumen_clases)
        prop_muestra=float(input("[+] Ingrese proporción del tamaño de la muestra (valor entre 0.0 y 1.0):"))
        dataframe.conformar_modelo(prop_muestra)
        const_k=int(input("[+] Ingresa constante K: "))
        prec_train, prec_test=dataframe.entrenar_modelo(const_k)
        resultados=[["Precisión Interv. entrenamiento","Precisión Interv. Testeo"],[prec_train,prec_test]]
        print(tabulate(resultados,tablefmt="fancy_grid"))

        print("===== Modelo Optimizado =====")
        max_prec, max_k=dataframe.optimizar_k()

        optimizados=[["Mejor precisión del sistema", "Valor K"],[max_prec,max_k]]
        print(tabulate(optimizados,tablefmt="fancy_grid"))
        print("[OUT] Fin del programa.")
    except ValueError:
        print("[!] El atributo no se encuentra en el dataframe")

if __name__=="__main__":
    csv=buscar_csv()
    kn_neighbors(csv)