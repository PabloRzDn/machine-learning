# Algoritmos de Python utilizados en Machine Learning
---
### Resumen

En este repositorio se almacenan y actualizan algunos de los scripts (de elaboración propia) utilizados en diversos modelos matemáticos de Machine Learning; todo lo anterior, en el marco del curso [*Machine Learning with Python*](https://www.coursera.org/learn/machine-learning-with-python) desde donde se obtienen los dataframes para el desarrollo en cuestión.


*Machine Learning* corresponde a un subcampo de las ciencias informáticas, el cual otorga a los computadores la habilidad de resolver problemas sin ser instruidos explícitamente, sino que a través de la aplicación de modelos.

Dentro de las clasificaciones de algoritmos se encuentran:

- **Algoritmos Supervisados**: supone un conjunto de datos de partida, etiquetados previamente. Algunos ejemplos son:
    - Regresión (lineal, múltiple, no lineal).
    - Clasificación.
- **Algoritmos no supervisados**: parte de datos no etiquetados previamente, dentro de los que se pueden enumerar:
    - Clustering
    - Reducción de Dimensión
    - Estimación de Densidad.



### Contenido

Los scripts contenidos en este repositorio constan de un programa principal y un módulo, en el cual se incorporan todas las clases y métodos para la ejecución, según el tipo de proceso.

Se utilizan las librerias:
- Pandas
- numpy
- matplotlib
- sklearn

Los programas realizados abordan las siguientes técnicas:

- Regresión Lineal simple y múltiple (linear_multiple_regression) 
- Regresión Polinomial (polynomial_regression) 
- K-Nearest Neighbors 
- Árboles de decisión (decision_trees) (**EN DESARROLLO**)


